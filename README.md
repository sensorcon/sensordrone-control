Sensordrone-Control
===================

The first official Sensordrone App from Sensorcon

For this app to compile, you will need to add several libraries. Here are the versions we used

1) Version 1.3.2 of the Sensordrone Android Library;

Jars available at http://archiva.sensorcon.com/

Source available at http://bitbucket.sensorcon.com/sensordrone-android-library

2) androidplot-core-0.6.0
The Androidplot library, available at http://androidplot.com/

